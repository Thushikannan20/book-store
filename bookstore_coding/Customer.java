import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class Customer extends JFrame {

	private JPanel contentPane;
	private JTextField usertext;
	private JTextField passtext;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Customer frame = new Customer();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Customer() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel formlable = new JLabel("Login Form");
		formlable.setBounds(156, 57, 117, 16);
		contentPane.add(formlable);
		
		JLabel userlable = new JLabel("User name:");
		userlable.setBounds(58, 108, 87, 16);
		contentPane.add(userlable);
		
		JLabel passlable = new JLabel("Password:");
		passlable.setBounds(58, 137, 72, 16);
		contentPane.add(passlable);
		
		usertext = new JTextField();
		usertext.setBounds(141, 105, 132, 22);
		contentPane.add(usertext); 
		
		JButton loginbtn = new JButton("Login");
		loginbtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String Username = usertext.getText();
				String Password = passtext.getText();
				
				if(Username.contains("king") && Password.contains("one")) {
				passtext.setText(null);
				usertext.setText(null);
				}
				else
				{JOptionPane.showMessageDialog(null, "Invalid Login Details", "Login error", JOptionPane.ERROR_MESSAGE);
				passtext.setText(null);
				usertext.setText(null);
				}
			}
		});
		loginbtn.setBounds(152, 182, 97, 25);
		contentPane.add(loginbtn);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(142, 137, 131, 22);
		contentPane.add(passwordField);
	}
}
