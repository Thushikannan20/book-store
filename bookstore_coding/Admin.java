import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class Admin extends JFrame {

	private JPanel contentPane;
	private JTextField UserText;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Admin frame = new Admin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Admin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel AdminLabel = new JLabel("Admin page");
		AdminLabel.setBounds(140, 13, 88, 16);
		contentPane.add(AdminLabel);
		
		JLabel UserLable = new JLabel("Username:");
		UserLable.setBounds(59, 72, 71, 16);
		contentPane.add(UserLable);
		
		UserText = new JTextField();
		UserText.setBounds(131, 69, 116, 22);
		contentPane.add(UserText);
		UserText.setColumns(10);
		
		JLabel PasswordLable = new JLabel("Password:");
		PasswordLable.setBounds(44, 117, 71, 16);
		contentPane.add(PasswordLable);
		
		JButton Login1btn = new JButton("Login");
		Login1btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String Username = UserText.getText();
				String Password = passwordField.getText();
				
				if(Username.contains("king") && Password.contains("one")) {
				passwordField.setText(null);
				UserText.setText(null);
				}
				else
				{JOptionPane.showMessageDialog(null, "Invalid Login Details", "Login error", JOptionPane.ERROR_MESSAGE);
				passwordField.setText(null);
				UserText.setText(null);
				}
				
			}
		});
		Login1btn.setBounds(119, 170, 97, 25);
		contentPane.add(Login1btn);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(131, 114, 116, 22);
		contentPane.add(passwordField);
	}

}
